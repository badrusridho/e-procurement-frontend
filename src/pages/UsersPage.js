import React from "react";
import adminLayout from "../hoc/adminLayout";
import ModalComponent from "../components/ModalComponent";

class UsersPage extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            groupuserdata: [],
            person: [],
            username:'',
            email:'',
            firstname:'',
            lastname:'',
            contactnumber:'',
            nik:'',
            npwp:'',            
            stsedit:'0',
            password:'12345678',
            group_users:'',
            iduser : 0,
            add: false,
            edit: false,
            delete: false,
            approve: false,
            ignore: false,
            export: false
        };
        this.handleSave = this.handleSave.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    componentDidMount() {
        this.UserList();
        this.GroupUserList();
        const usAccess = JSON.parse(localStorage.getItem('UsAccess'));
        const MenuAcces = usAccess.filter(i => i.urlto == 'users');
        this.setState({
            add : MenuAcces[0].madd,
            edit : MenuAcces[0].medit,
            delete : MenuAcces[0].mdelete,
            approve : MenuAcces[0].mapprove,
            ignore : MenuAcces[0].mignore,
            export : MenuAcces[0].mexport
        })
    }
      
    UserList() {
        fetch('http://localhost/eprocurment-api/public/api/users/view')
        .then((response) => response.json())
        .then((data) => this.setState({person : data['data']}))
    }
      
    GroupUserList() {
        fetch('http://localhost/eprocurment-api/public/api/groupusers/view')
        .then((response) => response.json())
        .then((data) => this.setState({groupuserdata : data['data']}))
    }

    viewedit(item){
        this.setState({
            username : item.user_name,
            firstname : item.firstname,
            lastname : item.lastname,
            email : item.email,
            contactnumber : item.phone,
            nik : item.id_card,
            stsedit : '1',
            iduser : item.id,
            group_users : item.group_users_id
        })
    }

    adddata(){
        this.setState({
            username:'',
            email:'',
            firstname:'',
            lastname:'',
            contactnumber:'',
            nik:'',
            npwp:'',            
            stsedit:'0',
            group_users:'',
            iduser : 0
        })
    }

    delete(iduser){
        this.setState({
            iduser : iduser
        })          
    }
    handleDelete(event){
        const usToken = localStorage.getItem('UsToken'); 
                   
        fetch("http://localhost/eprocurment-api/public/api/users/delete", {
            method: "post",
            body: JSON.stringify({
                iduser: this.state.iduser,
                token: usToken
            }),
            headers:{
                'Content-Type': 'application/json'
            }
        })
        .then(() => {
            const data = this.state.person.filter(i => i.id !== this.state.iduser)
            this.setState({person : data})
            console.log("Deleted succeeded with  response");
            alert("Deleted Succes");
        })
        .catch(function(error) {
            console.log("Deleted failed", error);
        });
        // this.setState({person})
    }

    handleSave(event){
        event.preventDefault();
            const username = this.state.username;
            const email = this.state.email;
            const password = this.state.password;
            const usToken = localStorage.getItem('UsToken');    
            // const datsim = { username, email, password }
            // console.log(datsim);
            if(this.state.stsedit == '1'){
                fetch("http://localhost/eprocurment-api/public/api/users/update", {
                    method: "post",
                    body: JSON.stringify({
                        id: this.state.iduser, 
                        username: username, 
                        email: email, 
                        password: password, 
                        id_card: this.state.nik,
                        firstname: this.state.firstname,
                        lastname: this.state.lastname,
                        phone: this.state.contactnumber,
                        groupuser: this.state.group_users,
                        token: usToken
                    }),
                    headers:{
                        'Content-Type': 'application/json'
                    }
                })
                .then(() => {
                    this.UserList();
                    console.log("Request succeeded with  response");
                    alert("Updated Succes");
                })
                .catch(function(error) {
                    console.log("Request failed", error);
                });
            }else{                
                fetch("http://localhost/eprocurment-api/public/api/users/register", {
                    method: "post",
                    body: JSON.stringify({
                        username: username, 
                        email: email, 
                        password: password, 
                        id_card: this.state.nik,
                        firstname: this.state.firstname,
                        lastname: this.state.lastname,
                        phone: this.state.contactnumber,
                        groupuser: this.state.group_users,
                        token: usToken
                    }),
                    headers:{
                        'Content-Type': 'application/json'
                    }
                })
                .then(() => {
                    this.UserList();
                    console.log("Request succeeded with  response");
                    alert("Save Succes");
                })
                .catch(function(error) {
                    console.log("Request failed", error);
                });
            }
    }

    modalFooterDeleted(){      

        return <>
            <div style={{width:"100%"}}>
                <button className="btn btn-danger" onClick={this.handleDelete}
                    data-bs-dismiss="modal">Yes</button>
            &nbsp;
            <button className="btn btn-primary"
                data-bs-dismiss="modal">No</button>
            </div>
        </>;
    }

    modalDeleted(){
        return <>
            <div className="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab" tabIndex="0">
                <form>
                    <div className="mb-3">
                        <label htmlFor="group_name" className="form-label">Apakah anda yakin ingin menghapus data?</label>
                    </div>
                </form>
            </div>
        </>;
    }
    
    modalFooterContent(){      

        return <>
            <div style={{width:"100%"}}>
                <button className="btn btn-default" onClick={this.handleSave}
                    data-bs-dismiss="modal">Save</button> 
            </div>
        </>;
    }

    modalContent(){
        const groupuserdata = this.state.groupuserdata.map((item, i) => (
            <option value={ item.id }>{ item.group_name }</option>
        ));
        return <>
            <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li className="nav-item" role="presentation">
                    <button className="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">User Data</button>
                </li>
                {/* <li className="nav-item" role="presentation">
                    <button className="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Assign Roles</button>
                </li> */}
            </ul>
            <div className="tab-content" id="pills-tabContent">
                <div className="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab" tabIndex="0">
                    <form>
                        {/* <div className="mb-3 form-check form-switch">
                            <input className="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckChecked" />
                            <label className="form-check-label" htmlFor="flexSwitchCheckChecked">Assign For User</label>
                        </div> */}
                        <div className="mb-3">
                            <label htmlFor="exampleInputusername" className="form-label">Username</label>
                            <input type="text" className="form-control" id="username" aria-describedby="Username"
                                onChange={(e) => this.setState({username : e.target.value})}
                                value={ this.state.username }/>
                        </div>
                        {/* <div className="mb-3">
                            <label htmlFor="exampleInputEmail1" className="form-label">Customer Name</label>
                            <input type="text" className="form-control" id="customername" aria-describedby="emailHelp"/>
                        </div> */}
                        <div className="mb-3">
                            <label htmlFor="exampleInputfirstname" className="form-label">First Name</label>
                            <input type="text" className="form-control" id="firstname" aria-describedby="firstname"
                                onChange={(e) => this.setState({firstname : e.target.value})}
                                value={ this.state.firstname }/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="exampleInputlastname" className="form-label">Last Name</label>
                            <input type="text" className="form-control" id="lastname" aria-describedby="lastname"
                                onChange={(e) => this.setState({lastname : e.target.value})}
                                value={ this.state.lastname }/>
                        </div>
                        <div className="mb-3">
                            <label className="form-check-label" htmlFor="male">Gender</label>
                            &nbsp;
                            <input type="radio" name="radios" className="form-check-input" id="male" disabled="" />
                            &nbsp;<label className="form-check-label" htmlFor="male">Male</label>
                            &nbsp;&nbsp;
                            <input type="radio" name="radios" className="form-check-input" id="female" disabled="" />
                            &nbsp;<label className="form-check-label" htmlFor="female">Female</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="contactnumber" className="form-label">Contact Number</label>
                            <input type="text" className="form-control" id="contactnumber" aria-describedby="contactnumber" 
                                onChange={(e) => this.setState({contactnumber : e.target.value})}
                                value={ this.state.contactnumber }/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="email" className="form-label">Email address</label>
                            <input type="email" className="form-control" id="email" aria-describedby="emailHelp"
                                onChange={(e) => this.setState({email : e.target.value})} 
                                value={ this.state.email }/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="exampleInputcontactnumber" className="form-label">NIK</label>
                            <input type="text" className="form-control" id="nik" aria-describedby="nik" 
                                onChange={(e) => this.setState({nik : e.target.value})}
                                value={ this.state.nik }/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="exampleInputcontactnumber" className="form-label">NPWP</label>
                            <input type="text" className="form-control" id="npwp" aria-describedby="npwpp" 
                                onChange={(e) => this.setState({npwp : e.target.value})}
                                value={ this.state.npwp }/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="validationServer04" className="form-label">Group Users</label>
                            <select className="form-select is-invalid" id="validationServer04" required=""
                                onChange={(e) => this.setState({group_users : e.target.value})}>
                                <option disabled="" value="0">Choose...</option>
                                { groupuserdata }
                            </select>
                        </div>
                    </form>
                </div>
                {/* <div className="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab" tabIndex="0">
                </div> */}
            </div>
        </>;
    }

    render(){
        var no = 1;
        const persons = this.state.person.map((item, i) => (

            <tr>
                <td>{ no++ }</td>
                <td>{ item.user_name }</td>
                <td>{ item.firstname } { item.lastname }</td>
                <td>{ item.email }</td>
                <td>&nbsp;</td>
                <td>{ item.group_name }</td>
                <td>{ item.status }</td>
                <td>
                    <div className="dropdown table-action-dropdown">
                        <button className="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButtonSM" data-bs-toggle="dropdown" aria-expanded="false"><i className="fa fa-ellipsis-v" aria-hidden="true"></i></button>
                        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButtonSM">
                            {this.state.edit ? (
                                <li><a className="dropdown-item" href="#" onClick={this.viewedit.bind(this, item)}
                                data-bs-toggle="modal" data-bs-target="#exampleModalDefault">
                               <i className="fa fa-pencil" aria-hidden="true"></i>&nbsp;Edit</a></li>
                            ) : (
                                ''
                            )}
                            <div className="dropdown-divider"></div>        
                            {this.state.delete ? (
                                <li><a className="dropdown-item text-danger" href="#" onClick={this.delete.bind(this, item.id)}
                                data-bs-toggle="modal" data-bs-target="#deletedModal">
                                <i className="fa fa-trash" aria-hidden="true"></i>&nbsp;Delete</a></li>
                            ) : (
                                ''
                            )}                    
                            
                        </ul>
                    </div>
                </td>
            </tr>
        ));
        
        return (
            <>
            
            <div>
                <div className="bd-example">
                <nav aria-label="breadcrumb">
                <ol className="breadcrumb">
                    <li className="breadcrumb-item"><a href="#">User Management</a></li>
                    <li className="breadcrumb-item"><a href="#">Users</a></li>
                </ol>
                </nav>
                </div>
            </div>
            <div className="table-container">
                <div className="row">
                    <div className="col-12">
                        {/* <button className="btn btn-primary" type="button">Add User</button> */}
                        {this.state.add ? (
                            <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModalDefault"
                                onClick={this.adddata.bind(this)}>
                                Add User
                            </button>
                        ) : (
                            ''
                        )}

                        <ModalComponent title="Deleted Confirmation" footerContent={this.modalFooterDeleted()} content={this.modalDeleted()} dataBsBackdrop="static" id="deletedModal"/>
                        <ModalComponent title="Add Users" footerContent={this.modalFooterContent()} content={this.modalContent()} dataBsBackdrop="static" id="exampleModalDefault"/>
                    </div>
                </div>
                                    
                <div className="row">
                    <div className="col-12">
                        &nbsp;
                    </div>
                </div>
                <div className="d-flex text-muted">
                    <table className="table">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Username</th>
                                <th>Full Name</th>
                                <th>Email</th>
                                <th>Customer Code</th>
                                <th>Role Level</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            { persons }
                        </tbody>
                    </table>
                </div>
                <nav className="table-bottom-center-pagination" aria-label="Page navigation example ">
                    <ul className="pagination">
                        <li className="page-item">
                        <a className="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span className="sr-only">Previous</span>
                        </a>
                        </li>
                        <li className="page-item"><a className="page-link" href="#">1</a></li>
                        <li className="page-item"><a className="page-link" href="#">2</a></li>
                        <li className="page-item"><a className="page-link" href="#">3</a></li>
                        <li className="page-item">
                        <a className="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span className="sr-only">Next</span>
                        </a>
                        </li>
                    </ul>
                </nav>
            </div>
            </>
          );
    }
}

export default adminLayout(UsersPage);