import React from "react";
import "../../assets/css/login.css"
import { Link } from 'react-router-dom';
import authLayout from "../../hoc/authLayout";

class LoginPage extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            username:'',
            password:'',
            isLogin:''
        };
    }


    handleLogin(event){
        event.preventDefault();
            const username = this.state.username;
            const password = this.state.password;
            // const datsim = { username, email, password }
            // console.log(datsim);
        fetch("http://localhost/eprocurment-api/public/api/auth/login", {
            method: "post",
            body: JSON.stringify({
                user_name: username, 
                password: password, 
            }),
            headers:{
                'Content-Type': 'application/json'
            }
        }).then(response => response.json())
        .then((response) => {
            // this.UserList();
            // console.log(response['token']);
            if(response['status'] === false){
                alert(response['message']);
            }else{
                localStorage.setItem('UsToken', response['token']);
                window.location = "/home";
                // this.props.history.push("/home");
                console.log("Request succeeded with  response");
            }
        })
        .catch(function(error) {
            // this.setState({isLogin : ""});
            console.log("Request failed", error);
        });
    }

    render(){

        return <>
            <form className="login-form">
                <div className="d-flex align-items-center my-4">
                    <h1 className="text-center fw-normal mb-0 me-3">Sign In</h1>
                </div>
                {/* <!-- Email input --> */}
                <div className="form-outline mb-4">
                    {/* <label className="form-label" htmlFor="form3Example3">Email address</label> */}
                    <input type="email" id="form3Example3" className="form-control form-control-lg"
                    placeholder="Enter Username" onChange={(e) => this.setState({username : e.target.value})}/>
                </div>

                {/* <!-- Password input --> */}
                <div className="form-outline mb-3">
                    {/* <label className="form-label" htmlFor="form3Example4">Password</label> */}
                    <input type="password" id="form3Example4" className="form-control form-control-lg"
                    placeholder="Enter password" onChange={(e) => this.setState({password : e.target.value})}/>
                </div>

                <div className="form-outline mb-3">
                    {/* <!-- Checkbox --> */}
                    {/* <div className="form-check mb-0">
                    <input className="form-check-input me-2" type="checkbox" value="" id="form2Example3" />
                    <label className="form-check-label" htmlFor="form2Example3">
                        Remember me
                    </label>
                    </div> */}
                    <Link to="/reset-password" className="text-body">Forgot password?</Link>
                </div>

                <div className="text-center text-lg-start mt-4 pt-2">
                    <Link to="#" type="button" className="btn btn-primary btn-lg"
                     onClick={this.handleLogin.bind(this)} >Login</Link>
                    <p className="small fw-bold mt-2 pt-1 mb-0">Now To E-Procurement? <a href="#!"
                        className="link-danger">Pendaftaran Rekanan</a></p>
                </div>
            </form>
        </>
    }
}

export default authLayout(LoginPage);